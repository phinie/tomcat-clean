# Use it!

	FROM phimar/tomcat-clean

	COPY SomeWebapp.war $CATALINA_HOME/webapps/ROOT.war

	CMD ["catalina.sh", "run"]

# Build it!

	docker build .
	
# Run it!

	docker run -p 8080:8080 <imageIdFromBuild>